# -*- coding: utf-8 -*-
__version__ = "0.3"

import os
from math import ceil, e, floor

import fitz  # import for PyMuPDF
import numpy as np
from loguru import logger

# config:
keywords = [
    "direct",
    "scope1",
    "scope2",
    "scope3",
    "scope 1",
    "scope 2",
    "scope 3",
    "GHG",
    "305-1",
    "305-2",
]
unit_hints = [
    "carbon dioxide",
    "carbon dioxide equivalent",
    "CO2",
    "CO 2",
    "CO2e",
    "CO2 e",
    "CO2-e",
    "CO2eq",
    "CO2 eq",
    "CO2-eq",
    "CO2equivalent",
    "CO2 equivalent",
    "CO2-equivalent",
]


# splits string of integer number in thousands
def ___split_str_in_thou(string):
    """Splits string of integer number into thousands
    >>> ___split_str_in_thou("1234567890")
    ['1', '234', '567', '890']
    """
    # fill with zeros from left (needed for zip hack later)
    string = string.zfill(ceil(len(string) / 3) * 3)
    # separate value before comma into thousands
    string = [
        a + b + c for a, b, c in zip(string[-3::-3], string[-2::-3], string[-1::-3])
    ][::-1]
    string[0] = string[0].lstrip("0")
    return string


# This function generates all possible ways a value can be represented as a string.
# It returns a tuple of the string represenations and the resprective float values
def __generate_value_repr(value):
    """ Generates all possible string representation of a float value
    >>> __generate_value_repr(9123)[0]
    ['9123.0', '9123. 0', '9123,0', '9123, 0', '9123.', '9123,', '9123', '9 123.0',\
 '9 123. 0', '9 123,0', '9 123, 0', '9 123.', '9 123,', '9 123', '9,123.0', '9,123. 0',\
 '9,123.', '9,123', '9, 123.0', '9, 123. 0', '9, 123.', '9, 123', '912.3', '912. 3',\
 '912,3', '912, 3', '91.23', '91. 23', '91,23', '91, 23', '9.123', '9. 123']
    """
    logger.trace("Starting generation of value representations")
    # generate string from float
    _value = np.format_float_positional(value)

    number_parts = _value.split(".")
    number_parts[0] = ___split_str_in_thou(number_parts[0])

    # catching the (nonexistent) edgecase that <1kg is emitted
    number_parts[0][0] = number_parts[0][0] if number_parts[0][0] else "0"

    separators = [["", " "], [".", ". "], [",", ", "]]  # possible separators

    repr_arr = []
    value_arr = []

    oom = int(np.log10(value)) + 1 if value != 0 else 1

    logger.trace("Starting iteration over OOMs")
    # iterate over all ooms
    for o in range(oom):
        number_parts[1] = number_parts[1].rstrip("0")
        values_before_comma = number_parts[0]
        value_after_comma = number_parts[1] if number_parts[1] else "0"

        has_decimal_places = not (value_after_comma == "0")
        has_thousands_separator = len(values_before_comma) > 1

        # iterate over thousands separators
        for j in range(len(separators) if has_thousands_separator else 1):
            # choose comma separators
            comma_sep = {
                0: [
                    item
                    for i, sublist in enumerate(separators)
                    if i > 0
                    for item in sublist
                ],
                1: separators[2],
                2: separators[1],
            }[j]

            # values whose interpretation can be confused (9,123 == 9.123 or 9123) are
            # assumed to be anglosaxon locale (9123), so skip generation of 9.123 with
            # "," separator
            if not has_thousands_separator and len(value_after_comma) == 3:
                comma_sep = separators[1]

            # if we cant distinguish the number from a comma number, don't use the "."
            # separator as a thousands seaparator. This indistinguishability occurs if
            # the number looks like 9123 (no decimal places, 1 thousands separator and
            # nonzero last value of number in front of comma; 9.120 is 9120 not 9.12)
            not_discernable_from_comma_front = (len(values_before_comma) == 2) and (
                values_before_comma[1][-1] != "0"
            )
            if not_discernable_from_comma_front and not has_decimal_places and j == 1:
                continue

            # we only want one iteration if there is no thousands separator to print
            sep_styles_to_print = separators[j] if has_thousands_separator else [""]

            # iterate over separator styles
            for k in sep_styles_to_print:
                _tmp = k.join(values_before_comma)

                for sep in comma_sep:
                    repr_arr.append(_tmp + sep + value_after_comma)
                    value_arr.append(
                        "".join(values_before_comma) + "." + value_after_comma
                    )

                if not has_decimal_places:
                    # only use non-spaced separator
                    repr_arr.append(_tmp + comma_sep[0])
                    value_arr.append("".join(values_before_comma))
                    # append other comma separator for non-thousand separated number
                    if j == 0:
                        repr_arr.append(_tmp + comma_sep[2])
                        value_arr.append("".join(values_before_comma))
                    # print raw number without thousands separators
                    repr_arr.append(_tmp)
                    value_arr.append("".join(values_before_comma))

        # move comma, but not if order of magnitude is one (prevents exceptions)
        if len(number_parts[0]) == 1 and len(number_parts[0][0]) == 1:
            break
        else:
            number_parts[1] = values_before_comma[-1][-1] + number_parts[1]
            number_parts[0] = ___split_str_in_thou("".join(values_before_comma)[:-1])

    logger.trace("Returning from generation of value representations")
    return repr_arr, np.array(value_arr)


def __concat_unit_strings(base_unit, si_specifier, oom_strings, sep=" "):
    unit_strings_concatenated = []
    for spec in si_specifier:
        if spec == "":
            for oom_str in oom_strings:
                unit_strings_concatenated.append(
                    (oom_str + sep + base_unit).lstrip(" ").rstrip(" ")
                )
        else:
            for oom_str in oom_strings:
                unit_strings_concatenated.append(
                    (oom_str + sep + spec + sep + base_unit).lstrip(" ").rstrip(" ")
                )
                unit_strings_concatenated.append(
                    (oom_str + spec + sep + base_unit).lstrip(" ").rstrip(" ")
                )
    return unit_strings_concatenated


def __generate_unit_repr(value, found_value_repr):
    value = float(value)
    found_value_repr = float(found_value_repr)
    unit_oom = value / found_value_repr if found_value_repr != 0 else 1
    logger.trace(f"unit_oom: {unit_oom}")

    base_units_t = [
        "t",
        "ton",
        "tons",
        "tonnes",
    ]  # Note that mass of 1 tons != 1 tonnes, but not too far off
    # TODO: include kg: base_units_kg = ["kg", "kilograms"]
    si_specifier = ["metric", "m", ""]
    kilo_oom_str = ["k", "kilo", "thousand", "thousands of"]
    mega_oom_str = ["M", "Mio.", "mio", "million", "millions of", "mega"]
    giga_oom_str = ["G", "giga", "billion", "billions of"]

    unit_repr_list = []

    for base_unit in base_units_t:
        if base_unit == "t":
            if int(unit_oom) == 1000:
                unit_repr_list += __concat_unit_strings(
                    base_unit, si_specifier, kilo_oom_str, sep=""
                )
            elif int(unit_oom) == 1000000:
                unit_repr_list += __concat_unit_strings(
                    base_unit, si_specifier, mega_oom_str, sep=""
                )
            elif int(unit_oom) == 1000000000:
                unit_repr_list += __concat_unit_strings(
                    base_unit, si_specifier, giga_oom_str, sep=""
                )
            else:
                unit_repr_list += __concat_unit_strings(
                    base_unit, si_specifier, [""], sep=""
                )
        else:
            if int(unit_oom) == 1000:
                unit_repr_list += __concat_unit_strings(
                    base_unit, si_specifier, kilo_oom_str
                )
            elif int(unit_oom) == 1000000:
                unit_repr_list += __concat_unit_strings(
                    base_unit, si_specifier, mega_oom_str
                )
            elif int(unit_oom) == 1000000000:
                unit_repr_list += __concat_unit_strings(
                    base_unit, si_specifier, giga_oom_str
                )
            else:
                unit_repr_list += __concat_unit_strings(base_unit, si_specifier, [""])
    if "t" in unit_repr_list:
        unit_repr_list.remove("t")
    logger.trace(f"unit_repr_list: {unit_repr_list}")
    return unit_repr_list


def _save_results(loc_dict, page, _repr, findings):
    for _r, _locations, _found_repr in zip(_repr, findings, list(map(bool, findings))):
        if _found_repr:
            cont_rect = page.rect * page.derotation_matrix
            loc_dict[page.number][_r] = [
                _l for _l in _locations if cont_rect.contains(_l)
            ]


# returns the number of pages of the extracted pdf
def convert_pdf(doc, savepath, year, value):
    insightful_pages = []
    to_delete_insightful_idx = []
    value_locations = {}
    keyword_locations = {}
    year_locations = {}
    unit_locations = {}
    unit_hint_locations = {}

    logger.trace("Starting Coarse Search")
    # Iterate over all pages in document and search for occurrences of the
    # keywords together with value -> Collect numbers of possible insightful pages in
    # list. First reduce value to relevant digits
    _value = str(value).replace(".", "")
    _value = _value.strip("0")
    year_repr = [
        str(year),
        "FY" + str(year)[2:] + " ",
    ]  # the + " " is added to prevent finding of FY20XX in case of year is 2020
    year_repr += ["".join([y + " " for y in year])[:-1] for year in year_repr]
    value_repr, value_arr = __generate_value_repr(value * 1e3)
    for page in doc:
        text = page.getTextPage().extractText()
        text = text.replace(",", "").replace(".", "").replace(" ", "")
        if (
            (_value in text)
            and (
                any(map(page.searchFor, keywords))
                or any([_kw in text for _kw in keywords])
            )
            and (
                any(map(page.searchFor, year_repr))
                or any([_y in text for _y in year_repr])
            )
        ):  # searchFor is case insensitive
            logger.trace(
                "Coarse Search: Found keyword, value, and year on page"
                + f" {page.number}..."
            )
            insightful_pages.append(page.number)

    if len(insightful_pages) == 0:
        logger.trace("Coarse Search: Did not find keywords and value on same page")
    else:
        # Iterate over all pages in document and search for occurrences of the
        # keywords together with value -> Collect numbers of possible insightful pages
        # in list.
        logger.trace("Starting Fine Search")
        value_repr, value_arr = __generate_value_repr(value * 1e3)
        for i, page_no in enumerate(insightful_pages):
            page = doc.loadPage(page_no)
            value_findings = list(map(page.searchFor, value_repr))
            keyword_findings = list(map(page.searchFor, keywords))
            year_findings = list(map(page.searchFor, year_repr))
            unit_hint_findings = list(map(page.searchFor, unit_hints))

            if (
                any(value_findings) and any(keyword_findings) and any(year_findings)
            ):  # searchFor is case insensitive
                logger.trace(
                    "Fine Search: Found keyword, year, and value on page"
                    + f" {page.number}..."
                )
                value_locations[page_no] = {}
                keyword_locations[page_no] = {}
                year_locations[page_no] = {}
                unit_locations[page_no] = {}
                unit_hint_locations[page_no] = {}

                cont_rect = page.rect * page.derotation_matrix

                # save value and corresponding unit search results
                for _value, _locations, _found_value in zip(
                    value_arr, value_findings, list(map(bool, value_findings))
                ):
                    if _found_value:
                        value_locations[page_no][_value] = [
                            _l for _l in _locations if cont_rect.contains(_l)
                        ]

                        unit_reprs = __generate_unit_repr(value, _value)
                        unit_findings = list(map(page.searchFor, unit_reprs))
                        for _unit, _locations, _found_unit in zip(
                            unit_reprs, unit_findings, list(map(bool, unit_findings))
                        ):
                            if _found_unit:
                                _locs = [
                                    _l for _l in _locations if cont_rect.contains(_l)
                                ]
                                try:
                                    unit_locations[page_no][_unit] += _locs
                                except KeyError:
                                    unit_locations[page_no][_unit] = _locs

                # save year search results
                _save_results(year_locations, page, year_repr, year_findings)

                # save keyword search results
                _save_results(keyword_locations, page, keywords, keyword_findings)

                # save unit hint search results
                _save_results(unit_hint_locations, page, unit_hints, unit_hint_findings)

                # kick out doubles (either for all or for unit_hints especially)
                for _dict in [
                    value_locations,
                    year_locations,
                    unit_locations,
                    unit_hint_locations,
                ]:
                    keys_reversed = list(
                        reversed(sorted(list(_dict[page_no].keys()).copy(), key=len))
                    )
                    for idx, _key in enumerate(keys_reversed):
                        for __key in keys_reversed[idx + 1 :]:
                            # if a shorter key is substring of this key, they might
                            # have found the same result
                            if __key in _key:
                                for _loc in _dict[page_no][_key]:
                                    for _idx, __loc in enumerate(_dict[page_no][__key]):
                                        # delete double result from list
                                        # checks using fitz.Rect.contains
                                        # if __loc in _loc:
                                        #    del _dict[page_no][__key][_idx]
                                        if (
                                            (__loc.x0, __loc.y0) == (_loc.x0, _loc.y0)
                                        ) or (
                                            (__loc.x1, __loc.y1) == (_loc.x1, _loc.y1)
                                        ):
                                            del _dict[page_no][__key][_idx]
            else:
                logger.trace(
                    f"Did not find results in fine search on page {page.number}"
                )
                to_delete_insightful_idx.append(i)

        insightful_pages = [
            x
            for i, x in enumerate(insightful_pages)
            if i not in to_delete_insightful_idx
        ]

        if len(insightful_pages) == 0:
            logger.trace("Fine Search: Did not find keywords and value on same page")
        else:
            # Set range of pages to insightful ones and save edited document. Options
            # are set to keep file size as small as possible.
            doc.select(insightful_pages)
            doc.save(
                savepath,
                garbage=4,
                deflate=True,
                deflate_images=True,
                deflate_fonts=True,
            )

    return (
        value_locations,
        keyword_locations,
        year_locations,
        unit_locations,
        unit_hint_locations,
    )


if __name__ == "__main__":
    pass
