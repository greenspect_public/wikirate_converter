import datetime
import glob
import json
import multiprocessing
import os
import pickle
import re
import sys
import time
from pathlib import Path
from shutil import rmtree

import fitz
import numpy as np
import pandas as pd
import requests
from bs4 import BeautifulSoup
from loguru import logger
from requests.exceptions import ConnectionError, HTTPError, RequestException, Timeout

try:
    from .pdf_converter import convert_pdf
except ImportError:
    pass

fitz.TOOLS.mupdf_display_errors(False)


class HTMLException(Exception):
    pass


class LengthException(Exception):
    pass


class SourceException(Exception):
    pass


class FitzPDF(fitz.fitz.Document):
    def __init__(self, *args, **kwargs):
        super(FitzPDF, self).__init__(*args, **kwargs)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        super(FitzPDF, self).close()


def purge_previous_data(path):
    logger.info("Purging data from previous run")
    rmtree(path, ignore_errors=True)
    Path(path).mkdir(parents=True)


# This function inserts consecutive numbers if the file names already exist.
# The number is inserted before the suffix if it exists, else at the end.
def __filename_unique(filename):
    # prepare format string
    if filename.endswith(".pdf"):
        _tmp = [*filename]
        _tmp.insert(-4, "{}")
        filename = "".join(_tmp)
    else:
        filename = filename + "{}"

    # check if file exists. If yes, iterate over all existing numbered files until a
    # new file can be created
    if os.path.isfile(filename.format("")):
        counter = 1
        while os.path.isfile(filename.format("_" + str(counter))):
            counter += 1
        filename = filename.format("_" + str(counter))
    else:
        filename = filename.format("")

    return filename


# This function tries to download a PDF from the given url to the given file name. It
# handles the cases where the URL is a HTML file (e.g. from WikiRate) or the download is
# empty. It returns the filename the PDF was saved under.
def _download_pdf(url, filename):
    response = requests.get(
        url,
        timeout=5,
    )

    if len(response.content) == 0:
        raise LengthException(f"PDF is of size 0")

    # If HTML is received from WikiRate, extract the download link and download this,
    # else throw exception.
    ct = response.headers["content-type"]
    if ct.startswith("text/html") and url.startswith("https://wikirate.org/"):
        soup = BeautifulSoup(response.content, features="html.parser")
        links = soup.find("span", {"class": "pr-3 download-source-link"})
        try:
            url = links.a["href"]
        except KeyError:
            raise SourceException("Error finding href")

        response = requests.get(
            url,
            timeout=5,
        )

        if len(response.content) == 0:
            raise LengthException("PDF is of size 0")
    elif ct.startswith("text/html"):
        raise HTMLException("type is text/html")

    # make filename unique using suffixes
    filename = __filename_unique(filename)

    # write file to unique filename
    with open(filename, "wb") as _file:
        _file.write(response.content)

    return filename


# This wrapper function tries to download from the given url. It handles a number of
# exceptions and returns a non-zero return value if any exception occurred. It also
# returns the actual filename (with suffixed number), the PDF was saved under.
def _download_wrapper(url, filename):
    try:
        filename = _download_pdf(url, filename)
    except HTTPError as err_h:
        logger.opt(colors=True).debug(f"<red>HTTP error occurred: {err_h}</red>")
        raise err_h
    except ConnectionError as err_c:
        logger.opt(colors=True).debug(f"<red>Connection error occurred: {err_c}</red>")
        raise err_c
    except Timeout as err_t:
        logger.opt(colors=True).debug(f"<red>Timeout error occurred: {err_t}</red>")
        raise err_t
    except RequestException as err_r:
        logger.opt(colors=True).debug(f"<red>Request error occurred: {err_r}</red>")
        raise err_r
    except HTMLException as err_ht:
        logger.opt(colors=True).debug(f"<red>Found HTML instead of PDF: {err_ht}</red>")
        raise err_ht
    except LengthException as err_l:
        logger.opt(colors=True).debug(f"<red>Download was empty: {err_l}</red>")
        raise err_l
    except SourceException as err_s:
        logger.opt(colors=True).debug(
            f"<red>Could not find source on WikiRate: {err_s}</red>"
        )
        raise err_s
    return filename


# renders the pages from doc using informations from page_nos, value_locations into
# outfile
def draw_findings(
    doc,
    value_locations_by_page,
    keyword_locations_by_page,
    year_locations_by_page,
    unit_locations_by_page,
    unit_hint_locations_by_page,
    outfile,
):
    for page, p_n in zip(doc, value_locations_by_page):
        for value in value_locations_by_page[p_n]:
            for loc in value_locations_by_page[p_n][value]:
                page.drawRect(loc, color=(1, 0.306, 0.365), width=1.9)
        for kw in keyword_locations_by_page[p_n]:
            for rect in keyword_locations_by_page[p_n][kw]:
                page.drawRect(rect, color=(0.243, 0.549, 0.906), width=1.9)
        for year in year_locations_by_page[p_n]:
            for rect in year_locations_by_page[p_n][year]:
                page.drawRect(rect, color=(0.165, 0.710, 0.353), width=1.9)
        for unit in unit_locations_by_page[p_n]:
            for rect in unit_locations_by_page[p_n][unit]:
                page.drawRect(rect, color=(1.000, 0.000, 1.000), width=1.9)
        for uh in unit_hint_locations_by_page[p_n]:
            for rect in unit_hint_locations_by_page[p_n][uh]:
                page.drawRect(rect, color=(0.600, 0.000, 1.000), width=1.9)
        mat = fitz.Matrix(4, 4)
        pix_map = page.getPixmap(matrix=mat, clip=page.rect)
        pix_map.writePNG(outfile + f"_pg_{p_n:03d}.png")


# returns the oom of value wrt kg_value
def _get_oom(kg_value, value):
    if float(value) == 0.0:
        return 1
    frac = int(kg_value) / float(value)
    return int(round(np.log10(frac)))


class WikiRateConverter:
    def __init__(self, df, filename):
        self.filename = filename
        self.datapath = os.path.dirname(self.filename)
        self.reportpath = os.path.join(self.datapath, "reports")
        self.extractpath = os.path.join(self.datapath, "extracts")
        self.imagepath = os.path.join(self.datapath, "img")

        # prepare environment
        Path(self.reportpath).mkdir(parents=True, exist_ok=True)
        Path(self.extractpath).mkdir(parents=True, exist_ok=True)
        Path(self.imagepath).mkdir(parents=True, exist_ok=True)

        for _col in [
            "AnswerPage",
            "Metric",
            "Company",
            "SourcePage",
            "OriginalSource",
            "Comments",
            "dl_url",
            "dl_filename",
            "ex_file",
            "ex_pages",
            "ex_resultfile",
        ]:
            try:
                df.loc[:, _col] = df[_col].astype("str")
            except KeyError:
                pass

        for _col in ["Year", "SourceCount", "ex_pn"]:
            try:
                df.loc[:, _col] = df[_col].astype("int64")
            except KeyError:
                pass

        self._emiss_data = df

    def download_pdfs(self, force=True):
        self._emiss_data.loc[:, "dl_url"] = ""
        self._emiss_data.loc[:, "dl_filename"] = ""

        logger.info("Starting Downloads.")
        # remove previous downloads
        purge_previous_data(self.reportpath)

        dl_success = 0
        tot_start = datetime.datetime.now()
        # iterate over all entries
        for idx, elem in self._emiss_data.iterrows():
            # check if data was already downloaded
            if any(self._emiss_data.dl_url == elem.OriginalSource):
                self._emiss_data.at[idx, "dl_url"] = elem.OriginalSource
                dl_fn = self._emiss_data[
                    self._emiss_data.dl_url == elem.OriginalSource
                ].dl_filename
                dl_fn.reset_index(inplace=True, drop=True)
                try:
                    self._emiss_data.at[idx, "dl_filename"] = dl_fn[0]
                except TypeError:
                    self._emiss_data.at[idx, "dl_filename"] = dl_fn
                logger.debug(
                    "Already downloaded report for "
                    + f"{elem.Company} from {elem.Year} from {elem.dl_url}, skipping!"
                )

            elif any(self._emiss_data.dl_url == elem.SourcePage):
                self._emiss_data.at[idx, "dl_url"] = elem.SourcePage
                dl_fn = self._emiss_data[
                    self._emiss_data.dl_url == elem.SourcePage
                ].dl_filename
                dl_fn.reset_index(inplace=True, drop=True)
                try:
                    self._emiss_data.at[idx, "dl_filename"] = dl_fn[0]
                except TypeError:
                    self._emiss_data.at[idx, "dl_filename"] = dl_fn
                logger.debug(
                    "Already downloaded report for "
                    + f"{elem.Company} from {elem.Year} from {elem.dl_url}, skipping!"
                )

            # download report
            else:
                # try to download from original source first to reduce load on WikiRate
                # servers
                if type(elem.OriginalSource) == str and elem.OriginalSource != "nan":
                    url = elem.OriginalSource
                else:
                    url = elem.SourcePage

                logger.debug(
                    f"Getting report for {elem.Company} from {elem.Year} from {url}"
                )

                def __format(string):
                    return "_".join(re.findall(r"\w+", string))

                start = time.time()
                try:
                    filename = _download_wrapper(
                        url,
                        os.path.join(
                            self.reportpath,
                            f"{__format(elem.Company.lower())}_{elem.Year}.pdf",
                        ),
                    )
                except Exception as e:
                    if type(elem.OriginalSource) == str:
                        url = elem.SourcePage
                    else:
                        continue

                    logger.debug(f"Trying to download from WikiRate Url: {url}")
                    try:
                        filename = _download_wrapper(
                            url,
                            os.path.join(
                                self.reportpath,
                                f"{__format(elem.Company.lower())}_{elem.Year}.pdf",
                            ),
                        )
                    except Exception as e:
                        logger.error(
                            f"Failed WikiRate download for {elem.Company}, {elem.Year}"
                        )
                        continue

                dl_success += 1
                logger.opt(colors=True).debug(
                    f"<green>Download was successful! Took {time.time()-start:.1f}"
                    " seconds.</green>"
                )

                self._emiss_data.at[idx, "dl_url"] = url
                self._emiss_data.at[idx, "dl_filename"] = filename

        logger.success(
            f"Successfully downloading {dl_success}/{len(self._emiss_data)} reports took"
            f" {str(datetime.datetime.now()-tot_start)}."
        )

        return self._emiss_data

    def extract_pages(self, force=False):
        if force or ("ex_resultfile" not in self._emiss_data.columns):
            self._emiss_data.loc[:, "ex_file"] = ""
            self._emiss_data.loc[:, "ex_pages"] = ""
            self._emiss_data.loc[:, "ex_pn"] = -1
            self._emiss_data.loc[:, "ex_resultfile"] = ""

        def scrub(reportfile, return_dict, company, year):
            try:
                with FitzPDF(reportfile) as doc:
                    try:
                        doc.scrub(redact_images=2)
                    except RuntimeError as e:
                        logger.error(
                            "Encountered Exception whilst scrubbing,"
                            f"aborting conversion of {company}, {year} PDF!"
                        )
                        return_dict["ret"] = 1
                        return
                    except KeyboardInterrupt as e:
                        logger.error(
                            "Caught KeyboardInterrupt,"
                            f"aborting conversion of {company}, {year} PDF!"
                        )
                        return_dict["ret"] = 1
                        return
                    else:
                        doc.save(
                            reportfile[:-4] + "_scrub.pdf",
                            garbage=4,
                            deflate=True,
                            deflate_images=True,
                            deflate_fonts=True,
                        )
                        return_dict["ret"] = 0
                return
            except RuntimeError:
                logger.error(f"Could not open PDF of {company}, {year}!")
                return_dict["ret"] = 1
                return

        # remove previous extractions
        purge_previous_data(self.extractpath)

        tot_start = datetime.datetime.now()
        ret_val = 0
        for idx, elem in self._emiss_data.iterrows():
            reportfile = elem.dl_filename

            try:
                extractfile = os.path.join(
                    self.extractpath, os.path.basename(reportfile)
                )
            except TypeError:
                logger.error(
                    f"Report was not downloaded for {elem.Company}, {elem.Year}"
                    + ", skipping."
                )
                ret_val += 1
                continue

            if os.path.isfile(extractfile) and (not force):
                logger.info("Extraction exists, skipping!")
                continue

            logger.info(f"Converting {reportfile}")
            start = time.time()
            if not os.path.isfile(reportfile):
                logger.warning(
                    f"Report for {elem.Company} was not downloaded, skipping!"
                )
                continue

            # Open the PDF using PyMuPDF:
            logger.debug("Scrubbing PDF")
            manager = multiprocessing.Manager()
            return_dict = manager.dict()

            # Scrub images and potentially dangerous content
            p = multiprocessing.Process(
                target=scrub, args=(reportfile, return_dict, elem.Company, elem.Year)
            )
            p.start()
            # Wait for 10 seconds or until process finishes
            p.join(10)

            if p.is_alive():
                logger.error("Scrubbing timed out, aborting this conversion!")
                # Terminate - may not work if process is stuck for good
                p.terminate()
                if p.is_alive():
                    p.kill()
                p.join()
                ret_val += 1
                continue
            elif return_dict["ret"]:
                logger.error("Error during scrubbing, aborting this conversion!")
                ret_val += 1
                continue

            with FitzPDF(reportfile[:-4] + "_scrub.pdf") as doc:
                logger.debug("Extracting pages from PDF")
                try:
                    (
                        value_locations_by_page,
                        keyword_locations_by_page,
                        year_locations_by_page,
                        unit_locations_by_page,
                        unit_hint_locations_by_page,
                    ) = convert_pdf(doc, extractfile, elem.Year, elem.Value)
                except RuntimeError as e:
                    logger.error(
                        "Encountered Runtime Exception, aborting this conversion!"
                    )
                    ret_val += 1
                    continue
                except KeyboardInterrupt as e:
                    logger.error("Caught KeyboardInterrupt, aborting this conversion!")
                    ret_val += 1
                    continue

            found_val = ""
            for p_n in value_locations_by_page:
                _sep = ","
                found_val = _sep.join(value_locations_by_page[p_n])
                oom_list = list(
                    map(
                        _get_oom,
                        [elem.Value * 1e3] * len(value_locations_by_page[p_n]),
                        list(value_locations_by_page[p_n].keys()),
                    )
                )
                logger.trace(f"Page {p_n}:")
                logger.trace(f"Found {found_val}")
                logger.trace(f"With OOM (kg) {_sep.join(map(str, oom_list))}")

            logger.trace(f"Searched for {elem.Value}")
            num_pages = len(value_locations_by_page)
            if num_pages > 0:
                logger.opt(colors=True).debug(
                    f"<green>Extracted {num_pages} page"
                    + ("s" if num_pages > 1 else "")
                    + "</green>"
                )
                self._emiss_data.at[idx, "ex_pn"] = num_pages
            else:
                logger.opt(colors=True).debug("<cyan>Did not find any matches</cyan>")
                ret_val += 1
                continue

            self._emiss_data.at[idx, "ex_file"] = extractfile
            self._emiss_data.at[idx, "ex_pages"] = ",".join(
                [str(x) for x in value_locations_by_page.keys()]
            )

            # dump search results
            resultfile = os.path.join(
                self.extractpath,
                os.path.basename(elem.dl_filename).split(".")[0] + ".result",
            )
            self._emiss_data.at[idx, "ex_resultfile"] = resultfile
            result_obj = {
                "value_locations_by_page": value_locations_by_page,
                "keyword_locations_by_page": keyword_locations_by_page,
                "year_locations_by_page": year_locations_by_page,
                "unit_locations_by_page": unit_locations_by_page,
                "unit_hint_locations_by_page": unit_hint_locations_by_page,
            }
            with open(resultfile, "wb") as f:
                pickle.dump(result_obj, f)
            logger.info(f"Took {time.time() - start:.1f} s.")

        success_pages = len(self._emiss_data) - ret_val
        logger.success(
            f"Successfully extracting relevant pages for"
            f" {success_pages}/{len(self._emiss_data)} reports took"
            f" {str(datetime.datetime.now()-tot_start)}."
        )

        return self._emiss_data

    def draw_extractions(self):
        for idx, elem in self._emiss_data.iterrows():
            if os.path.isfile(elem.ex_resultfile):
                result_obj = pickle.load(open(elem.ex_resultfile, "rb"))

                (
                    value_locations_by_page,
                    keyword_locations_by_page,
                    year_locations_by_page,
                    unit_locations_by_page,
                    unit_hint_locations_by_page,
                ) = result_obj.values()
            else:
                logger.error(
                    f"Extraction does not exist for {elem.Company}, {elem.Year}"
                    + ", skipping."
                )
                continue

            with FitzPDF(elem.ex_file) as doc:
                draw_findings(
                    doc,
                    value_locations_by_page,
                    keyword_locations_by_page,
                    year_locations_by_page,
                    unit_locations_by_page,
                    unit_hint_locations_by_page,
                    os.path.join(self.imagepath, os.path.basename(elem.ex_file)[:-4]),
                )


if __name__ == "__main__":
    from pdf_converter import convert_pdf

    # config options
    numreports = 25
    new_data = False
    datapath = "../data/"

    # file from previous run does not exist
    if os.path.isfile(datapath + "emiss_data.csv") and not new_data:
        emiss_data = pd.read_csv(datapath + "emiss_data.csv", index_col=0)
    else:
        # get emissions files and sort by scope
        files_by_scope = glob.glob(datapath + "*_*v.csv")

        # read data from files
        emiss_data = []
        for _file in files_by_scope:
            emiss_data.append(pd.read_csv(_file))

        # merge data to single DF
        emiss_data = pd.concat(emiss_data, ignore_index=True)
        emiss_data.columns = emiss_data.columns.str.replace(" ", "")

        # remove entries which were calculated from numbers in reports, as these can't
        # be extracted by the NN
        prob_calc_entries = emiss_data.Comments.str.lower().str.contains(
            "|".join(["calc", "sum", "\+", "combined"])
        )
        logger.info(
            f"Filtered out {prob_calc_entries.sum() / len(emiss_data) * 100}% of"
            " elements because they are probably calculated."
        )
        emiss_data = emiss_data[np.logical_not(prob_calc_entries)]

    if isinstance(emiss_data.index, pd.MultiIndex):
        emiss_data.reset_index(inplace=True)
    emiss_data = emiss_data.iloc[:numreports]

    wr_converter = WikiRateConverter(
        emiss_data, os.path.dirname(os.path.abspath(__file__))
    )

    emiss_data = wr_converter.download_pdfs()
    emiss_data = wr_converter.extract_pages()
    wr_converter.draw_extractions()
